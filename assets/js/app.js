$(function(){
    var $ul        = $('#slide');
    var $ulBullets = $('#bull');
    var moviesAPI  = "https://api.themoviedb.org/3/movie/upcoming?api_key=e082a5c50ed38ae74299db1d0eb822fe";

    $.getJSON(moviesAPI, {format:"json"})
    .done(function(data){
        $.each(data.results, function(i, movies){
            var $math     = Math.round(this.vote_average/2);
            var $overview = this.overview;
            var moviesNb  = data.results;
            
            if($overview.length > 70){
                $('.overview').text($overview.substring(0,70) + ' Lire la suite ...');
                } 
            var $li = $(
                '<li class="" id="sliders"><div class="eb__nico_content"><h3>'+this.title+'</h3><p class="overview">'+$overview+'</p><div class="eb__nico_stars" id="#stars"><div class="stars-' + $math +'"></div></div></div><img src=https://image.tmdb.org/t/p/w1000'+this.backdrop_path +' alt=""></li>' 
            );
            $li.appendTo($ul);

            var $liBullets = $('<li class="eb__bullets" id="bullet-'+moviesNb.length+'"></li> ');
            $liBullets.appendTo($ulBullets);
        });      
    
    })
    .fail(function(jqxhr, textStatus, error){
        var err = textStatus + ", " + error;
        alert('Le chargement de l\'api est un échec ' + err);
    });

    var currentSlide = 0;
    var slides = $('.eb__nico_sliders li');
    var slidesNbr = slides.prevObject["0"].images;
    var slidesAmt = slidesNbr;

   

    function slideCycle(){
        var slide = $('.eb__nico_sliders li').eq(currentSlide);
        slide.siblings('.active').removeClass('active');
        slide.addClass('active');

        var bullette = $('.eb__bullets_all li').eq(currentSlide);
        bullette.siblings('.active').removeClass('active');
        bullette.addClass('active');

    }
    
    var autoSlide = setInterval(function() {
        
        currentSlide += 1;
        if (currentSlide > slidesAmt - 1) {
            currentSlide = 0;
        }
        slideCycle();
      }, 2500);

     
    
      $('ul.right li').click(function(e){
        e.preventDefault();
        clearInterval(autoSlide);
        currentSlide += 1;
        if (currentSlide > slidesAmt - 1) {
            currentSlide = 0;
          }
          slideCycle();
      });

      $('ul.left li').click(function(e){
        e.preventDefault();
        clearInterval(autoSlide);
        currentSlide -= 1;
        if (currentSlide < 0) {
            currentSlide = slidesAmt -1;
          }
          slideCycle();
      });

    


    

    


});